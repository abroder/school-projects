# school-projects

A list of school projects that I'm particularly proud of, but can't share for
honor code (or similar) reasons. If you're interested in the work I did on any
of them, just let me know and I'm happy to talk about them!

## japanese-translator
*Winter 2013*

A simple rules-based translator from Japanese to English. Developed over the
course of a week with a partner for Stanford's CS124 (From Languages to
Information).

**Languages(s) & Frameworks**: Java

## cool-compiler
*Spring 2014*

A compiler for the Cool classroom programming language, written for the 
Stanford class CS143 (Compilers). Four projects across the course of a quarter,
each focused on a different phase of the compiler - lexical analysis, parsing, 
semantic analysis, and code generation. Worked with a partner.

**Language(s) & Frameworks**: C++

## koblt
*Winter - Spring 2015*

Koblt is a platform to enable developers to create asymmetric virtual reality 
experiences. It acts as a bridge between a web-based application and 3D 
software developed in the Unreal Engine, allowing the VR space to be 
dynamically changed and modified on the fly. Some possible use cases we were
particularly interested in include immersive training simulations and guided
learning experiences. Worked on a team of 5 people.

Koblt was developed with advising from a representative from Oculus as part of
Stanford’s CS210 (Software Project Experience with Corporate Partners) course. 

**Language(s) & Frameworks**: Javascript (Node.js), C++ (Unreal Engine)

## pintos
*Winter 2016*

An implementation of the [Pintos](https://en.wikipedia.org/wiki/Pintos) 
operating system for the Stanford class CS140 (Operating Systems and Systems
Programming). Implemented thread priority scheduling, user processes & system
calls, virtual memory (paging and memory mapped files), and a file system.
Worked on a team of 3 people.

More information about the Pintos assignments available at the 
[website](http://web.stanford.edu/class/cs140/projects/pintos/pintos.html).

**Language(s) & Frameworks**: C